# trainer

# data structure

```
instinct {
    conditionalStat: string // ex. health
    conditionalValue: num // ex. 50
    conditionalComparison // ex. greaterThan
    action // ex. attack
    endCondition //
}

pet {
    name: string
    maxMentalForce: num
    maxPhysicalForce: num
    endurancePerForceRation: num
    stamina: num
    maxMemory: num //number of directions (if then) to configure
    cardioThreshold: num //how much newtons per second can be conjured for free
    atpRecoveryRate: num //while not moving, how fast stamina recouperates
}
```

# steps

0. decide on battle encounter
1. decide on what makes a pet
1. decide on how battles are fought
1. decide on what automation is available
1. decide on gameplay loop

# calculated stats

1. mass (max force adds mass)
2. stamina exertion rate (calculated based on mass and endurance multiplier)
3. conjuring is calculated by applying force. the mass grows based on the mental force applied (and therefore as stamina is depleted)

# trackable

1. personal stats
2. distance from enemy
3. velocity of enemy
4. enemy mass (heavier, same, lighter)
5. enemy stamina (bottom third, middle third, top third)

# battle mechanics

1. in an effort to teach physics & allow for variability of gameplay (ie, using magical force to throw the trajectory of an attack instead of dodge), we assume the following:

- damage is calculated by the distance moved or Newtons/ force applied from outside force
- ranged attacks are made by first conjuring the mass, and then applying force
  - this can be abstracted as 'magical force'
  - if you have to decide between strength (which can increase force of melee), or mental strength/force, then range naturally sacrifices everything that physical strength gives you

## balance considerations

1. making ranged attacks will probably require multiple steps of 'if then'
   - if(not attacking) build magical mass
   - if (magical mass === 10 weight) apply magic force
2. eventually, people can group these "if then's" and name them (like a move), for easy understanding

# pet commands

1. move (build moment for collision, dodge)

2. build energy

3. push energy

# game pillars:

1. every gain must be accompanied with a sacrifice (gain strength, lose speed)

- this allows pets who have played 100 battles to be on similar ground as someone on new: same cumulative stats
- this also allows a shifting meta: since time played isn't the meta, and there is more balance, meta's might shift towards the weakness of the current meta
- provides incentive to selling: meta may have shifted, just because you have won, doesn't mean you will continue to win
- provides incentive to get multiple pets to build in different ways to try things out: trying to change your pet with a severe stat allocation may take longer than starting at base

2. each encounter has a winner and a loser where money is transfered (30 to winner, 50 from loser, 20 to transaction)

3. For games to be valuable they should either be intuitive (to use intuition to solve other problems (hopefully problems that build transferable knowledge)), or unintuitive but accurate to reality (to build new intuitions that are transferable)

# rules:

1. takes 24 hours to fully heal
2. buy a pet for x and they all start the same
3. money from wins is kept on pet
4. pet can be retired/destroyed to cash out money

# ideas for other games

1. you only choose how they behave, not how they grow

thought: magic is science if it can be described. the only way magic makes sense is if it is whimsical, reacts to thoughts and history. It requires a creator of the magic and one(s) who presumably can make inconsistent decisions to grant it to users

<!--
- strength - increases damage, increases speed
- awareness - decreases cost to dodge
- muscle memory/reactivity/intellectual capacity? - increases accuracy? counters awareness by making attack harder to dodge
- thickness - decreases damage taken, but adds to weight
- endurance - increases max stamina
- anxiety resistance/nerve/tolerance/mental strength? - this can slowly increase through the match and when it reaches max, it depletes stamina, and then the pet loses, this is a fale safe to two that just defend - also this can be used to affect effectiveness of everything
-->
